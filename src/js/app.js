import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready
  
  let products = document.querySelectorAll('.product');
  products.forEach(product => {
    let price = product.querySelector('.price');
    let priceValue = price?.textContent;
    product.setAttribute('data-price', priceValue);
  })
});